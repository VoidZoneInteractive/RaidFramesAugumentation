# Raid Frames Augumentation

## About

Addon to highlight top DPS in Blizzard Raid Frames in order to simplify Prescience application for Augumentation Evokers.

### Features:
- Highlight top four DPS with frame
- Frame shows status for Prescience - yellow when player don't have Prescience active, green otherwise

### Requirements
Addon requires Details! addon to be enabled and first window set to `Damage Dealers`.

### Known issues
- Currently it only takes first four places from Details!. If given player is not available (e.g. dead) it will skip him but not go to next player in list
- Configuration for plugin is essentially non-existient. My next step is to fix that.

## Author
GrzeGurz or Wichajster on Burning Legion EU.

## Credits
deezo - Author of CustomGlow library used in this addon
nevcairiel - Author of ButtonGlow library used as a base for deezo CustomGlow

## Useful informations

409311 - Prescience Spell ID
410089 - Spell Aura ID for Prescience