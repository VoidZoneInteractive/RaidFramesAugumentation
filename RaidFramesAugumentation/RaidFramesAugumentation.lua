print("Raid Frames Augumentation loaded!")

local addonLoaded = false
local inCombat = false

function createDpsRankNumber(rank)
    local raidUnitAugumentationRank = CreateFrame("Frame", "RaidUnitAugumentationRank" .. rank)

    raidUnitAugumentationRank:SetWidth(1)
    raidUnitAugumentationRank:SetHeight(1)
    raidUnitAugumentationRank:SetAlpha(.90);
    raidUnitAugumentationRank.text = raidUnitAugumentationRank:CreateFontString(nil,"ARTWORK")
    raidUnitAugumentationRank.text:SetFont("Fonts\\ARIALN.ttf", 13, "OUTLINE")
    raidUnitAugumentationRank.text:SetTextColor(0.95, 0.95, 0.32, 1 - (rank - 1) / 10)
    raidUnitAugumentationRank.text:SetText(rank)
    raidUnitAugumentationRank.text:Hide()
end

function attachRankToRaidUnit(raidUnit, rank)
    local raidUnitAugumentationRank = _G["RaidUnitAugumentationRank" .. rank]
    width, height = raidUnitAugumentationRank.text:GetSize()
    raidUnitAugumentationRank:SetFrameLevel(raidUnit:GetFrameLevel()+8)
    raidUnitAugumentationRank:SetPoint("BOTTOMLEFT", raidUnit, "BOTTOMLEFT", 0, 0)
    raidUnitAugumentationRank.text:SetPoint("BOTTOMLEFT",width / 2, height / 2)
    raidUnitAugumentationRank.text:Show()
    raidUnitAugumentationRank:SetParent(raidUnit)
end

local function onAddonLoad(self, event, ...)
    local loadedAddonName = ...

    if addonLoaded == false and 'Blizzard_RaidUI' == loadedAddonName then

        createDpsRankNumber(1)
        createDpsRankNumber(2)
        createDpsRankNumber(3)
        createDpsRankNumber(4)

        addonLoaded = true
    end
end;

function HideRank(rank)
    local raidUnitAugumentationRank = _G["RaidUnitAugumentationRank" .. rank]
    if raidUnitAugumentationRank ~= nil then
        raidUnitAugumentationRank.text:Hide()
    end
end

local function clearFrames(self, event, ...)
    for i = 1, 40 do
        local name, rank, subgroup = GetRaidRosterInfo(i)
        if name ~= nil then
            local raidFrameId = "CompactRaidGroup" .. subgroup .. "Member" .. (i % 5)
            local raidFrame = _G[raidFrameId]

            LibStub("LibCustomGlow-1.0").PixelGlow_Stop(raidFrame)
        end
    end

    HideRank(1)
    HideRank(2)
    HideRank(3)
    HideRank(4)
end

local function setColorByAura(name)
    local color = {0.95, 0.95, 0.32, 1}
    AuraUtil.ForEachAura(name, "HELPFUL", nil, function(auraName, icon, count, dispelType, duration, expirationTime, source, isStealable, nameplateShowPersonal, spellId)
        if spellId == 410089 then
            color = {0.32, 0.95, 0.32, 1}
            return true
        end
    end)

    return color
end

local function updateRaidFrameHighlight(self, event, ...)
    local firstEntryName = ''
    local detailsFirstEntry = _G["DetailsBarra_1_1"]
    if (detailsFirstEntry.minha_tabela ~= nil) then
        firstEntryName = detailsFirstEntry.minha_tabela['displayName']
    end

    local secondEntryName = ''
    local detailsSecondEntry = _G["DetailsBarra_1_2"]
    if (detailsSecondEntry.minha_tabela ~= nil) then
        secondEntryName = detailsSecondEntry.minha_tabela['displayName']
    end

    local thirdEntryName = ''
    local detailsThirdEntry = _G["DetailsBarra_1_3"]
    if (detailsThirdEntry.minha_tabela ~= nil) then
        thirdEntryName = detailsThirdEntry.minha_tabela['displayName']
    end

    local fourthEntryName = ''
    local detailsFourthEntry = _G["DetailsBarra_1_4"]
    if (detailsFourthEntry.minha_tabela ~= nil) then
        fourthEntryName = detailsFourthEntry.minha_tabela['displayName']
    end

    for i = 1, 40 do
        local name, rank, subgroup, level, class, fileName, zone, online, isDead, role, isML, combatRole = GetRaidRosterInfo(i)
        if name ~= nil then
            local raidUnitFrameId = "CompactRaidGroup" .. subgroup .. "Member" .. (i % 5)
            local raidUnit = _G[raidUnitFrameId]

            if raidUnit ~= nil then
                if (firstEntryName ~= '' and string.match(name, firstEntryName .. ".*") ~= nil and not isDead) then
                    local color = setColorByAura(name)
                    LibStub("LibCustomGlow-1.0").PixelGlow_Start(raidUnit, color)
                    attachRankToRaidUnit(raidUnit, 1)
                elseif (secondEntryName ~= '' and string.match(name, secondEntryName .. ".*") ~= nil and not isDead) then
                    local color = setColorByAura(name)
                    LibStub("LibCustomGlow-1.0").PixelGlow_Start(raidUnit, color)
                    attachRankToRaidUnit(raidUnit, 2)
                elseif (thirdEntryName ~= '' and string.match(name, thirdEntryName .. ".*") ~= nil and not isDead) then
                    local color = setColorByAura(name)
                    LibStub("LibCustomGlow-1.0").PixelGlow_Start(raidUnit, color)
                    attachRankToRaidUnit(raidUnit, 3)
                elseif (fourthEntryName ~= '' and string.match(name, fourthEntryName .. ".*") ~= nil and not isDead) then
                    local color = setColorByAura(name)
                    LibStub("LibCustomGlow-1.0").PixelGlow_Start(raidUnit, color)
                    attachRankToRaidUnit(raidUnit, 4)
                else
                    LibStub("LibCustomGlow-1.0").PixelGlow_Stop(raidUnit)
                end
            end
        end
    end
end

local function OnEvent(self, event, ...)
	if event == 'ADDON_LOADED' then
        onAddonLoad(self, event, ...)
	elseif event == 'PLAYER_ENTER_COMBAT' or event == 'PLAYER_REGEN_DISABLED' then
        inCombat = true
	elseif event == 'PLAYER_LEAVE_COMBAT' or event == 'PLAYER_REGEN_ENABLED' then
        inCombat = false
		clearFrames(self, event, ...)
	elseif (event == 'COMBAT_LOG_EVENT' or event == 'UNIT_SPELLCAST_SUCCEEDED' or event == 'UNIT_AURA') and inCombat == true then
		updateRaidFrameHighlight(self, event, ...)
	end
end

local main = CreateFrame("Frame")

main:RegisterEvent('ADDON_LOADED')
main:RegisterEvent('GROUP_ROSTER_UPDATE')
main:RegisterEvent('COMBAT_LOG_EVENT')
main:RegisterEvent('UNIT_SPELLCAST_SUCCEEDED')
main:RegisterEvent('PLAYER_ENTER_COMBAT')
main:RegisterEvent('PLAYER_REGEN_DISABLED')
main:RegisterEvent('PLAYER_LEAVE_COMBAT')
main:RegisterEvent('PLAYER_REGEN_ENABLED')
main:RegisterEvent('UNIT_AURA')

main:SetScript('OnEvent', OnEvent)
